import { RpcDefinition } from "@litbase/core";

// Every action needs a definition. You can call these actions on the frontend by referring to these definitions,
// and you can add logic that happens on the backend by referring to these same definitions.
export const customAction = new RpcDefinition<[], void>("customAction");
