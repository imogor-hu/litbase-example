import { createBinaryId, DocumentInterface } from "@litbase/core";
import { Collections } from "@my-app/common/models/collections";

export enum BlockType {
  HERO = "hero",
  EMPTY = "empty",
}

interface AbstractBlock {
  type: BlockType;
  title: string;
  id: number;
}

export interface HeroBlock extends AbstractBlock {
  type: BlockType.HERO;
  image: string;
}

export interface EmptyBlock extends AbstractBlock {
  type: BlockType.EMPTY;
}

export type Block = HeroBlock | EmptyBlock;

export interface Page extends DocumentInterface {
  url: string;
  blocks: Block[];
  name: string;
  slug: string;
  updatedAt: Date;
}

export function createEmptyPage(): Page {
  return {
    _id: createBinaryId(),
    _collection: Collections.PAGES,
    name: "",
    url: "",
    slug: "",
    blocks: [],
    updatedAt: new Date(),
  };
}
