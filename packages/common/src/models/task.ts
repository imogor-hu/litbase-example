import { BinaryId, DocumentInterface } from "@litbase/core";

export interface Task extends DocumentInterface {
  name: string;
  image: string;
  isDone: boolean;
  userId: BinaryId;
}
