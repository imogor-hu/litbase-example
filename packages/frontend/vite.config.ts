import { defineConfig } from "vite";
import reactRefresh from "@vitejs/plugin-react-refresh";
import reactSvgPlugin from "vite-plugin-react-svg";
import * as path from "path";
import dotenvFlow from "dotenv-flow";

dotenvFlow.config();

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [reactRefresh(), reactSvgPlugin()],
  resolve: {
    alias: {
      "@my-app/common": path.resolve(__dirname, "../common/src/"),
    },
    dedupe: ["react"],
  },
  esbuild: {
    jsxInject: `import React from 'react'`,
  },
  define: {
    "process.release": { ...process.release, name: "notnode" },
    "process.env": { ...process.env, SERVER_BASE_URL: "http://localhost:8082", NODE_ENV: "dev" },
  },
});
