import styled, { ThemeProvider } from "styled-components";
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import { Paths } from "../utils/paths";
import { User } from "@my-app/common/models/user";
import { isEmptyUser, isUserGuest } from "@litbase/core";
import { ModalProvider, roundedLg, spacing4, spacing5 } from "@litbase/alexandria";
import { TopScroller } from "@litbase/alexandria/components/utils/top-scroller";
import { ToastContainer } from "react-toastify";
import { useCurrentUser } from "@litbase/react";
import "tippy.js/dist/tippy.css";
import "react-toastify/dist/ReactToastify.css";
import * as Sentry from "@sentry/react";
import { Integrations } from "@sentry/tracing";
import { CaptureConsole } from "@sentry/integrations";
import { LoadingPage } from "./pages/loading-page";
import { theme, GlobalStyle } from "../styles/theme-types";
import { Admin } from "./pages/admin/admin";
import { LoginPage } from "./pages/auth/login-page";
import { RegisterPage } from "./pages/auth/register-page";
import { TodoPage } from "./pages/todo/todo-page";

export function App() {
  const user = useCurrentUser<User>();
  // The user object starts off as an empty user. If there's a logged in user, the user object will become the logged-in
  // user, otherwise it will become a Guest user.
  // While we don't know whether a user is logged in or not, we just display a loading page.
  if (!user || isEmptyUser(user)) {
    return <LoadingPage />;
  }
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <BrowserRouter>
        <TopScroller />
        <ModalProvider>
          <Row>
            <Col>
              <AuthSwitch />
            </Col>
          </Row>
          <StyledToastContainer autoClose={5000} />
        </ModalProvider>
      </BrowserRouter>
    </ThemeProvider>
  );
}

/** Switch between paths available to guest and logged in users. */
function AuthSwitch() {
  const user = useCurrentUser<User>();
  return (
    <>
      <Switch>{isUserGuest(user) ? <GuestOnlyPaths /> : <AuthPaths />}</Switch>
    </>
  );
}

function RedirectOnNoHit() {
  return <Redirect to={Paths.MAIN} />;
}

function GuestOnlyPaths() {
  return (
    <Switch>
      <Route path={"/login"} component={LoginPage} />
      <Route path={"/register"} component={RegisterPage} />
      <Redirect to={"/login"} />
    </Switch>
  );
}

function AuthPaths() {
  const user = useCurrentUser<User>();
  return (
    <Switch>
      <Route path={Paths.MAIN} exact component={TodoPage} />
      {user.isAdmin && <Route path={"/admin"} component={Admin} />}
      <RedirectOnNoHit />
    </Switch>
  );
}

const StyledToastContainer = styled(ToastContainer)`
  width: 26rem;

  .Toastify__toast--success,
  .Toastify__toast--error {
    font-family: inherit;
    background-color: white;
    border-radius: ${roundedLg};
    box-shadow: ${({ theme }) => theme.shadowLg};
    color: ${({ theme }) => theme.gray900};
    padding: ${spacing4} ${spacing5};

    .Toastify__close-button {
      color: ${({ theme }) => theme.gray600};
      line-height: 1;
      font-size: ${({ theme }) => theme.textXl};
      margin-top: -0.2rem;

      &:hover {
        color: ${({ theme }) => theme.gray700};
      }

      > svg {
        height: 1em;
        width: auto;
      }
    }
  }

  .Toastify__toast--success .Toastify__progress-bar {
    background-color: ${({ theme }) => theme.green300};
  }

  .Toastify__toast--error .Toastify__progress-bar {
    background-color: ${({ theme }) => theme.red600};
  }
`;

const Col = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
`;

const Row = styled.div`
  display: flex;
  max-height: 100vh;
`;
