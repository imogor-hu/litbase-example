import styled from "styled-components";
import { Button, size12, spacing4 } from "@litbase/alexandria";
import { breakPointsPixels } from "../../../styles/theme-types";
import { useQuery } from "@litbase/react/hooks/use-query";
import { Task } from "@my-app/common/models/task";
import { Collections } from "@my-app/common/models/collections";
import { client } from "@litbase/client";
import { NewTaskButton } from "./new-task-button";
import { Trash } from "@styled-icons/boxicons-regular";
import { Link } from "react-router-dom";
import { StyledIconBase } from "@styled-icons/styled-icon";
import { useCurrentUser } from "@litbase/react";
import { User } from "@my-app/common/models/user";
import { LoadingPage } from "../loading-page";

export function TodoPage() {
  const user = useCurrentUser<User>();
  // The userQuery hook returns data from the database. You can pretty much use it the same way you would use mongodb.
  const [tasks, loading, error] = useQuery<Task>(Collections.TASKS, {
    $matchAll: {
      userId: user._id,
    },
    // If $live is set to true, changes in the database will be instantly reflected in the 'tasks' variable.
    $live: true,
  });
  if (loading) {
    return <LoadingPage />;
  }
  if (error) {
    return <div>Oh no! Something bad happened!</div>;
  }
  return (
    <Body>
      <Content>
        {user.isAdmin && <Link to={"/admin"}>Admin</Link>}
        <Button $kind={"link"} onClick={() => client.logout()}>
          Logout
        </Button>
        <h1>To-do list</h1>
        <TaskList>
          {tasks.length === 0 && <EmptySpan>Add some tasks!</EmptySpan>}
          {tasks.map((elem) => (
            <TaskItem key={elem._id.value}>
              <span>
                {" "}
                <Image src={getDownloadUrl(elem.image)} />
                {elem.name}
              </span>
              <Trash onClick={() => client.litql.query(Collections.TASKS, { $match: { _id: elem._id }, $pull: 1 })} />
            </TaskItem>
          ))}
        </TaskList>
        <NewTaskButton />
      </Content>
    </Body>
  );
}

/** Uploaded files live on the server. By default you only get their relative path, so you need to combine that with
 *  the backend's path to get a working download url. */
function getDownloadUrl(relativePath: string) {
  return process.env.SERVER_BASE_URL + relativePath;
}

const EmptySpan = styled.span`
  margin-top: ${spacing4};
  margin-bottom: ${spacing4};
  color: gray;
`;

const Image = styled.img`
  width: ${size12};
  height: ${size12};
  object-fit: cover;
  border-radius: ${({ theme }) => theme.rounded};
  margin-right: ${spacing4};
`;

const TaskItem = styled.div`
  display: flex;
  padding: ${spacing4};
  justify-content: space-between;
  align-items: center;
  ${StyledIconBase} {
    color: red;
    font-size: ${({ theme }) => theme.textLg};
    cursor: pointer;
  }
`;

const TaskList = styled.div`
  display: flex;
  flex-direction: column;
  > * + * {
    border-top: 1px solid ${({ theme }) => theme.gray500};
  }
  button {
    width: fit-content;
  }
`;

const Content = styled.div`
  max-width: ${breakPointsPixels.xl}px;
  margin: 0 auto;
`;

const Body = styled.div`
  width: 100%;
  height: 100%;
  background: ${({ theme }) => theme.gray700};
  color: white;
  padding: ${spacing4};
`;
