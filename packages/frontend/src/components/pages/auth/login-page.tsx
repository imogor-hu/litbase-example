import { useCallback } from "react";
import { boolean, object, string } from "yup";
import { FormikLoginErrorAlert, LoginPageWrapper, LoginSubmitButton } from "./login-page-wrapper";
import { client } from "@litbase/client";
import { Helmet } from "react-helmet";
import { CheckboxField, FieldWrapper, FormikWrapper, InputField, timeout } from "@litbase/alexandria";
import { AuthRedirecter } from "./auth-redirecter";

function createEmptyEmailLoginObject() {
  return {
    email: "",
    password: "",
    rememberMe: true,
  };
}

export function LoginPage() {
  const onSubmit = useCallback((values: { email: string; password: string; rememberMe: boolean }) => {
    return timeout(100000, login(values), new Error(`A szerver nem válaszolt a rendelkezésre álló időn belül`));
  }, []);

  return (
    <>
      <Helmet>
        <title>Login</title>
      </Helmet>
      <LoginPageWrapper>
        <FormikWrapper
          initialValues={createEmptyEmailLoginObject()}
          onSubmit={onSubmit}
          validationSchema={emailLoginObjectSchema}
        >
          <>
            <FieldWrapper name="email" component={InputField} type="email" label="Email" placeholder="admin@email.hu" />
            <FieldWrapper
              name="password"
              component={InputField}
              type="password"
              label="Password"
              placeholder="●●●●●●●●"
            />
            <FieldWrapper name="rememberMe" component={CheckboxField} checkboxLabel="Remember Me" />
            <LoginSubmitButton>Login</LoginSubmitButton>
          </>
          <FormikLoginErrorAlert />
          <AuthRedirecter mode={"login"} />
        </FormikWrapper>
      </LoginPageWrapper>
    </>
  );
}

/** There's a variety of login options on the client. To use an e-mail and password authentication option, simply use
 *  loginWithEmailAndPassword */
async function login(values: { email: string; password: string }) {
  return await client.loginWithEmailAndPassword({
    email: values.email,
    password: values.password,
  });
}

const emailLoginObjectSchema = object().shape({
  email: string().required("Email address is required").email("Email address is not in the correct format"),
  password: string().required("Password required!"),
  rememberMe: boolean(),
});
