import styled from "styled-components";

export function NotFoundPage() {
  return (
    <Page>
      <h1>404</h1>
    </Page>
  );
}

const Page = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`;
