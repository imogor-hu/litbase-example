import { ReactNode } from "react";
import styled from "styled-components";
import { Button, size12, spacing2, spacing3, spacing4 } from "@litbase/alexandria";
import { Menu } from "@styled-icons/boxicons-regular";

export function AdminPageBody({ title, children }: { title: string; children: ReactNode }) {
  return (
    <Body>
      <HeaderRow title={title} />
      {children}
    </Body>
  );
}

export function HeaderRow({ title }: { title: string }) {
  return (
    <HeaderRowWrapper>
      <MenuButton />
      <Title>{title}</Title>
    </HeaderRowWrapper>
  );
}

export function MenuButton() {
  return (
    <MenuButtonWrapper className="sidebar">
      <Menu />
    </MenuButtonWrapper>
  );
}

const Title = styled.h3`
  font-size: ${({ theme }) => theme.textLg};
  margin: 0;
  display: flex;
  line-height: 1;
`;

const MenuButtonWrapper = styled.button`
  display: none;
  appearance: none;
  background: none;
  border: none;
  margin: 0 ${spacing2} 0 0;
  padding: 0;
  @media (max-width: 1000px) {
    display: flex;
    width: fit-content;
  }
  * {
    font-size: 1.8rem;
    color: ${({ theme }) => theme.primary900};
  }
`;

const HeaderRowWrapper = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: ${spacing4};
  background-color: ${({ theme }) => theme.primary50};
  z-index: 50;
  height: ${size12};
`;

const Body = styled.div`
  display: flex;
  flex-direction: column;
  padding: 0 ${spacing4};
  width: 100%;
  ${Button} {
    margin-top: ${spacing3};
  }
`;
