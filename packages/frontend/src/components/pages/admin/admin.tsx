import { AdminComponent } from "./common/admin-component";
import { newsEntity } from "./entities/news";
import logoPng from "./common/logo.png";
import { userEntity } from "./entities/user";
import { pagesEntity } from "./entities/pages";

export function Admin() {
  return <AdminComponent entities={[userEntity]} logoUrl={logoPng} name={"Starter admin"} prefix={""} />;
}
