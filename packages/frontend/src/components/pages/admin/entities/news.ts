import { News } from "@styled-icons/boxicons-regular";
import { ValueType } from "../common/types";
import { Collections } from "@my-app/common/models/collections";

export const newsEntity = {
  name: "news",
  humanReadableName: "News",
  icon: News,
  fields: {
    title: {
      humanReadableName: "Cím",
      type: ValueType.string,
      isIncludedInListPage: true,
    },
    content: {
      humanReadableName: "Tartalom",
      type: ValueType.richText,
      isIncludedInListPage: false,
    },
    image: {
      humanReadableName: "Kép",
      type: ValueType.file,
      multi: false,
    },
  },
  collectionName: Collections.NEWS,
  slugField: "title",
  searchableFields: ["title"],
  nameField: "title",
};
