import { createEmptyEntity, Entity } from "../common/types";
import { Card, FormCardTitle, FormikWrapper, spacing3, SubmitButton } from "@litbase/alexandria";
import { useHistory } from "react-router-dom";
import styled from "styled-components";
import { useAdminConfig } from "../common/admin-context";
import { AdminFormField } from "./admin-form-field";
import { useNewOrExistingDocument } from "../../../../hooks/use-new-or-existing-document";
import { saveModel } from "../../../../utils/save-model";
import { AdminPageBody } from "../common/admin-page-body";
import { Collections } from "@my-app/common/models/collections";

export function AdminEditPage({ id, entity }: { id: string; entity: Entity }) {
  const [item] = useNewOrExistingDocument(
    id,
    entity.collectionName as Collections,
    () => createEmptyEntity(entity),
    {}
  );
  const history = useHistory();
  const config = useAdminConfig();
  if (!item) {
    return null;
  }
  return (
    <AdminPageBody title={(item as unknown as Record<string, unknown>)[entity.nameField] as string}>
      <FormikWrapper
        initialValues={item}
        onSubmit={async (values) => {
          await saveModel({ values, slugField: entity.slugField, collection: entity.collectionName as Collections });
          history.push(config.prefix + "/admin/" + entity.name);
        }}
      >
        <Card>
          <FormCardTitle>Data</FormCardTitle>
          {Object.entries(entity.fields).map(([key, value], index) => (
            <AdminFormField value={value} name={key} key={index} />
          ))}
          <SaveButton>Save</SaveButton>
        </Card>
      </FormikWrapper>
    </AdminPageBody>
  );
}

const SaveButton = styled(SubmitButton)`
  margin-top: ${spacing3};
`;
