import { Plus } from "@styled-icons/boxicons-regular";
import Tippy from "@tippyjs/react";
import { Block, BlockType } from "@my-app/common/models/page";
import styled, { createGlobalStyle } from "styled-components";
import { SelectButton } from "./select-button";
import { BlockConfig } from "../../../common/types";
import { useMemo } from "react";
import { useBlockConfig } from "./block-context";

export function AddBlockButton({ onAdd }: { onAdd: (newBlock: Block) => void }) {
  const { blockConfig } = useBlockConfig();
  const options = useMemo(() => {
    return createAddBlockOptions(blockConfig);
  }, [blockConfig]);

  return (
    <SelectButton
      $kind="secondary"
      $size="sm"
      options={options}
      onSelect={(value) => onAdd(blockConfig[value].createEmpty(Date.now()))}
      header="Válaszd ki az új blokk típusát:"
    >
      <TippyStyles />
      <Plus />
      Új blokk
    </SelectButton>
  );
}

const TippyStyles = createGlobalStyle`
  .tippy-content {
    padding: 0;
  }
`;

const Img = styled.img`
  max-width: 500px;
`;

const Text = styled.span`
  width: 100%;
  display: block;
`;

const StyledTippy = styled(Tippy)`
  max-width: 550px !important;
  .tippy-box {
    max-width: 550px !important;
  }
`;

const createAddBlockOptions = (blockTypes: BlockConfig) =>
  Object.entries(blockTypes)
    .sort((a, b) => a[1].displayName.localeCompare(b[1].displayName))
    .map(([key, value]) => {
      return {
        label: (
          <StyledTippy placement="right" content={<Img src={value.explainerImage} />}>
            <Text>{value.displayName}</Text>
          </StyledTippy>
        ),
        value: key as BlockType,
      };
    });
