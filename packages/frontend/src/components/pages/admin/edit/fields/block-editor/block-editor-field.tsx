import { Page } from "@my-app/common/models/page";
import {
  FieldWrapper,
  FootCell,
  GridBody,
  HeadCell,
  InputField,
  ReorderableList,
  ReorderableListRow,
} from "@litbase/alexandria";
import { BlockConfig, BlockFieldType, createEmptyValue } from "../../../common/types";
import { AddBlockButton } from "./add-block-button";
import { FieldArray, FieldProps, FormikProps } from "formik";
import { BlockEntryRow } from "./block-entry-row";
import { BlockContext } from "./block-context";
import { AdminFormField } from "../../admin-form-field";
import { useMemo } from "react";

export function BlockEditorField({
  field,
  blockConfig,
}: FieldProps<Record<string, unknown>[]> & { field: any; blockConfig: BlockFieldType["config"] }) {
  const config = useMemo(() => {
    return createFinalBlockConfig(blockConfig);
  }, [blockConfig]);

  return (
    <BlockContext.Provider value={{ blockConfig: config }}>
      <FieldArray name={field.name}>
        {({ move, push, unshift, remove, form }) => {
          const blocks = (form as FormikProps<Page>).values[field.name];

          return (
            <ReorderableList columns="min-content max-content max-content 1fr min-content" onReordered={move}>
              {(provided) => (
                <>
                  <ReorderableListRow>
                    <HeadCell />
                    <HeadCell>Blokk neve</HeadCell>
                    <HeadCell>Blokk típusa</HeadCell>
                    <HeadCell>Link a blokkra</HeadCell>
                    <HeadCell>
                      <AddBlockButton onAdd={unshift} blockConfig={blockConfig} />
                    </HeadCell>
                  </ReorderableListRow>
                  <GridBody isEmpty={!blocks.length}>
                    {blocks.map((item, index) => (
                      <BlockEntryRow key={item.id} id={item.id} index={index} onRemove={remove} />
                    ))}
                    {provided.placeholder}
                  </GridBody>
                  <ReorderableListRow>
                    <FootCell $fullWidth>
                      <AddBlockButton onAdd={push} blockConfig={blockConfig} />
                    </FootCell>
                  </ReorderableListRow>
                </>
              )}
            </ReorderableList>
          );
        }}
      </FieldArray>
    </BlockContext.Provider>
  );
}

function createFinalBlockConfig(config: BlockFieldType["config"]): BlockConfig {
  return Object.fromEntries(
    Object.entries(config).map(([blockTypeName, value]) => [
      blockTypeName,
      {
        createEmpty: (id: number) => ({
          id,
          title: "",
          type: blockTypeName,
          ...createEmptyValue(value.fields),
        }),
        displayName: value.displayName,
        explainerImage: value.explainerImage,
        editor: (props: { name: string }) => <SingleBlockEditorField {...props} fields={value.fields} />,
      },
    ])
  );
}

function SingleBlockEditorField({
  name,
  fields,
}: {
  name: string;
  fields: BlockFieldType["config"]["name"]["fields"];
}) {
  return (
    <>
      <FieldWrapper name={name + ".title"} component={InputField} label={"Név"} />
      {Object.entries(fields).map(([key, value], index) => (
        <AdminFormField value={value} name={name + "." + key} key={index} />
      ))}
    </>
  );
}
