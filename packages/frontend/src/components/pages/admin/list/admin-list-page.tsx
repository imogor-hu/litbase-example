import { Entity, FieldType } from "../common/types";
import { AddButton, Cell, GridButton, GridRow, HeadCell, ConfirmDeleteButton } from "@litbase/alexandria";
import { useAdminConfig } from "../common/admin-context";
import { AdminListCell } from "./admin-list-cell";
import { AdminPageBody } from "../common/admin-page-body";
import { ListPage } from "./list-page";
import { ComponentProps } from "react";
import { Edit, Trash } from "@styled-icons/boxicons-solid";
import styled from "styled-components";
import Tippy from "@tippyjs/react";
import { Collections } from "@my-app/common/models/collections";
import { client } from "@litbase/client";

export function AdminListPage({ entity }: { entity: Entity }) {
  const config = useAdminConfig();
  const renderedFields = Object.entries(entity.fields).filter(([, value]) => !!value.isIncludedInListPage);
  return (
    <AdminPageBody title={entity.humanReadableName}>
      <ListPage
        type={entity.collectionName as Collections}
        searchFields={entity.searchableFields}
        buttons={
          <>
            <AddButton to={config.prefix + "/admin/" + entity.name + "/new"} />
          </>
        }
        fields={{}}
        rowComponent={(item, index) => (
          <GridRow $parity={index % 2} key={item._id.value}>
            {renderedFields.map(([key, value], index) => (
              <AdminListCell value={value as FieldType} item={item} fieldName={key} key={index} />
            ))}
            <Cell>
              <EditButton to={config.prefix + "/admin/" + entity.name + "/" + item._id.toUrlSafeString()} />
              <ConfirmDeleteButton
                content={"Are you sure you want to delete this item?"}
                onConfirm={() => client.litql.query(entity.collectionName, { $match: { _id: item._id }, $pull: 1 })}
              />
            </Cell>
          </GridRow>
        )}
        headCells={
          <GridRow>
            {renderedFields.map(([, value], index) => (
              <HeadCell key={-index}>{value.displayName}</HeadCell>
            ))}
            <HeadCell />
          </GridRow>
        }
        columns={`repeat(${renderedFields.length}, 1fr) min-content`}
      />
    </AdminPageBody>
  );
}

export function DeleteButton(props: ComponentProps<typeof GridButton>) {
  return (
    <Tippy content="Delete">
      <StyledGridButton {...props} $color="red800">
        <Trash />
      </StyledGridButton>
    </Tippy>
  );
}

export function EditButton(props: ComponentProps<typeof GridButton>) {
  return (
    <Tippy content="Szerkesztés">
      <StyledGridButton {...props} $color="primary500">
        <Edit />
      </StyledGridButton>
    </Tippy>
  );
}

const StyledGridButton = styled(GridButton)<{ $color: string }>`
  border: none;
  color: ${({ $color, theme }) => theme[$color]};
`;
