import { FontWeight } from "@litbase/alexandria";
import { StyledIconBase } from "@styled-icons/styled-icon";
import styled from "styled-components";
import { ReactNode } from "react";
import { SidebarProfile } from "./admin-sidebar-profile";
import { Accordion } from "../../../basic/accordion";
import { useAdminConfig } from "../common/admin-context";

export function AdminSidebarBody({ children }: { children: ReactNode }) {
  const config = useAdminConfig();
  return (
    <AdminSidebarWrapper>
      <StyledAdminSidebar>
        <SidebarHeader>{config.name}</SidebarHeader>
        <AccordionWrapper>
          <Accordion>{children}</Accordion>
        </AccordionWrapper>
        <SidebarProfile />
      </StyledAdminSidebar>
    </AdminSidebarWrapper>
  );
}

const AdminSidebarWrapper = styled.div`
  width: 23rem;
`;

const StyledAdminSidebar = styled.div`
  display: flex;
  flex-direction: column;
  background-color: ${({ theme }) => theme.gray800};
  font-size: ${({ theme }) => theme.textSm};
  min-width: 19rem;
  color: ${({ theme }) => theme.white};
  position: fixed;
  top: 0;
  left: 0;
  height: 100%;
  z-index: 99;
`;

const SidebarHeader = styled.div`
  display: flex;
  align-items: center;
  font-size: ${({ theme }) => theme.textXl};
  font-weight: ${FontWeight.Bold};
  padding: ${({ theme }) => theme.spacing6} ${({ theme }) => theme.spacing4};
  letter-spacing: 0.1em;

  > svg:first-child,
  > ${StyledIconBase}:first-child {
    margin-right: ${({ theme }) => theme.spacing3};
    height: ${({ theme }) => theme.text4xl};
    /* Fix svg not being properly centered */
    margin-bottom: -0.4rem;

    color: ${({ theme }) => theme.primary400};
    path {
      fill: ${({ theme }) => theme.primary400};
    }
  }
`;

const AccordionWrapper = styled.div`
  flex: 1 1 0;
  overflow: auto;
`;
