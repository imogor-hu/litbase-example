import styled from "styled-components";
import { LoadingIcon } from "@litbase/alexandria";

export function LoadingPage() {
  return (
    <StyledLoadingPage>
      <LoadingIcon />
    </StyledLoadingPage>
  );
}

const StyledLoadingPage = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  width: 100%;
  min-width: max-content;
  font-size: large;
  color: ${({ theme }) => theme.primary500};
  svg {
    max-width: 50vw;
    max-height: 50vh;
  }
`;
