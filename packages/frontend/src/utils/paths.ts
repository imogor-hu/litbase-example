export enum Paths {
  MAIN = "/",
  LOGIN = "/login",
  REGISTER = "/register",
  ADMIN_WELCOME = "/admin/welcome",
}
