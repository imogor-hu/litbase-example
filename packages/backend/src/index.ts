import { config, server } from "@litbase/server";
import { registerJoins } from "./database";
import path from "path";
import { AddressInfo } from "net";

config.redisUrl = process.env.REDIS_URL;
config.sessionSecret = process.env.SESSION_SECRET as string;

// Configure authentication providers
config.providers = {
  email: {
    fromAddress: process.env.EMAIL_FROM_ADDRESS as string,
    mailgun: {
      apiKey: process.env.MAILGUN_API_KEY as string,
      domain: process.env.MAILGUN_DOMAIN as string,
      apiDomain: process.env.MAILGUN_API_DOMAIN as string,
      validationApiKey: process.env.MAILGUN_VALIDATION_API_KEY,
    },
    captchaSecret: process.env.RECAPTCHA_SECRET as string,
  },
};

config.mongoOptions = {
  address: process.env.MONGO_URL as string,
  database: process.env.MONGO_DATABASE as string,
};

config.port = Number(process.env.SERVER_PORT);
config.address = process.env.SERVER_LISTEN_ADDRESS || undefined;

// Sets how files are stored
config.fileStorageType = "local";
config.fileStorage = {
  local: {
    path: path.resolve(process.env.UPLOAD_PATH || path.join(__dirname, "../uploads")),
  },
};
config.enableFileServer = true;

registerJoins();

(async function () {
  try {
    await server.startListening();
    console.debug("Litbase server is listening on ", (server.fastify.server.address() as AddressInfo)?.port);
  } catch (error) {
    console.error(error);
  }
})();
