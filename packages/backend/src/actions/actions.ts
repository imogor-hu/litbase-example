import { registry, RpcRequest } from "@litbase/core";
import { customAction } from "@my-app/common/actions/actions";

// Use the action definition to register logic for it. Whatever you return from here will arrive on the frontend.
registry.register(customAction, async (request: RpcRequest) => {
  return "Hello there!";
});
